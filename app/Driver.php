<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [
        'id','user_id','driver_id',

    ];
    public function order()
    {
        return $this->hasMany('App\Order');
    }
    
    
}
