<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [
        'user_id','address1','address2','city','state','country','mobile_no',
    ];
    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
