<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'id','order_id','price','quantity','category_info','cloth_info',

    ];
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    
}
