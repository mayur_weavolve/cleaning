<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClothPrice extends Model
{
    protected $fillable = [
        'id','category_id','cloth_id','price',

    ];
    public function category()
    {
      return $this->belongsTo(Category::class);
    }

    
}
