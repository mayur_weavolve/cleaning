<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'id','name','user_id','location','status','driver_id','price','release_date','release_status','latitude','longitude','pickup_date',

    ];
    public function orderitem()
    {
        return $this->hasMany('App\OrderItem');
    }
}
