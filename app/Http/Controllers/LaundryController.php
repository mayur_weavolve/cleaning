<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Laundry;
use Validator;
use Illuminate\Support\Facades\Auth;
class LaundryController extends Controller
{
    public function list()
    { 
        $user = Auth::user();
        $list = Laundry::where("user_id", $user->id)->paginate(5);
        $data=[];
        $data['laundries']=$list;
        return view('laundry_admin.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        $user = Auth::user();
        if($user->role_id=="1")
        {
            $list = Laundry::where('name','like',"{$serach}%")->paginate(5);
            $data=[];
            $data['laundries']=$list;
            return view('laundry_admin.list',$data);
        }
        else{
            $list = Laundry::where("user_id", $user->id)->where('name','like',"{$serach}%")->paginate(5);
            $data=[];
            $data['laundries']=$list;
            return view('laundry_admin.list',$data);
        }
    }
    public function add_laundry()
    { 
        return view('laundry_admin.add');
    }
    public function save_laundry(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'mobile_no' => 'required',
            'address' => 'required',  
            'latitude'=>'required',
            'longitude'=>'required',
            'status'=>'required',
        ]);
        if ($validator->fails()) {
            return redirect(route('add_laundry'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $laundry = new Laundry();
        $laundry->user_id =$user->id;
        $laundry->name = $data['name'];
        $laundry->mobile_no = $data['mobile_no'];
        $laundry->address = $data['address'];
        $laundry->latitude = $data['latitude'];
        $laundry->longitude = $data['longitude'];
        $laundry->status = $data['status'];
        $laundry->save();
        
        return redirect('laundry');
    }
    public function delete_laundry($id)
    {
        $delete=Laundry::find($id);
        $delete->delete();
        return redirect('laundry');
    }
    public function edit_laundry($id)
    {
        $list = Laundry::where('id',$id)->get();
        $data  = [];
        $data['laundries'] = $list;
        return view('laundry_admin.update',$data);
    }
    public function edit_save($id, Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'mobile_no' => 'required',
            'address' => 'required',  
            'latitude'=>'required',
            'longitude'=>'required',
            'status'=>'required',
        ]);
        if ($validator->fails()) {
            return redirect(route('add_laundry'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $laundry = Laundry::find($id);
        $laundry->name = $data['name'];
        $laundry->mobile_no = $data['mobile_no'];
        $laundry->address = $data['address'];
        $laundry->latitude = $data['latitude'];
        $laundry->longitude = $data['longitude'];
        $laundry->status = $data['status'];
        $laundry->save();
        return redirect('laundry');
    }
    public function user_laundry_list()
    { 
        $list = Laundry::get();
        $data=[];
        $data['laundries']=$list;
        return view('laundry_admin.list',$data);
    }
    
}
