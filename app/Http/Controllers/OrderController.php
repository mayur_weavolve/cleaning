<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function order_list()
    {
        $user = Auth::user();
        if($user->role_id == '1')
        {
            $list=Order::get();
            $data=[];
            $data['orders']=$list;
            return view('admin.order.list',$data);
        }
        else{
            $list=Order::where("user_id", $user->id)->get();
            $data=[];
            $data['orders']=$list;
            return view('admin.order.list',$data);
        }
    }
    public function order_item($id)
    {
        $list=Order::find($id);
        return view('admin.order.order_item',['order' => $list]);
    }
}
