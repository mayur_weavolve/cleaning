<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cloth;
use App\Category;
use App\ClothPrice;
use Validator;
class ClothController extends Controller
{
    public function cloth()
    {
        $list=Cloth::paginate(2);
        $data=[];
        $data['cloths']=$list;
        return view('admin.cloth.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        $list=Cloth::where('name','like',"{$serach}%")->paginate(2);
        $data=[];
        $data['cloths']=$list;
        return view('admin.cloth.list',$data);
    }
    public function add_cloth()
    {
        $list=Category::get();
        $data=[];
        $data['categories']=$list;
        return view('admin.cloth.add',$data);
    }
    public function save_cloth(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'description' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_cloth'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $cloths = new Cloth();
        $cloths->name = $data['name'];
        $cloths->description = $data['description'];
        if ($request->file('image') != null){
            $cloths->image = $request->file('image')->hashName('image');
            $request->file('image')
            ->store('image');
        }
        $cloths->save();
        if($cloths){
            $i = 0;
            $catgoryData = $data['categoryId']; 
            foreach($data['price'] as $price){
                $cloth_prices = new ClothPrice();
                $cloth_prices->cloth_id = $cloths->id;
                $cloth_prices->price = $price;
                $cloth_prices->category_id = $catgoryData[$i];
                $cloth_prices->save();
                $i++;
            }
        }
        return redirect('cloth');
    }
    public function update_cloth($id)
    {
        $cloths = Cloth::with('cloth_price')->where('id',$id)->get();
        $categories = Category::get();
        $clothPrices = ClothPrice::get();
        $data  = [];
        $data1 = [];
        $data['cloths'] = $cloths;
        $data1['categories'] = $categories;
        return view('admin.cloth.update',$data,$data1);
    }
    public function updatesave_cloth($id, Request $request){
        $data = $request->all();
        $cloths = Cloth::find($id);
        $cloths->name = $data['name'];
        $cloths->description = $data['description'];
        if ($request->file('image') != null){
           
            $cloths->image = $request->file('image')->hashName('image');
            $request->file('image')
            ->store('image'); 
        }
        $cloths->save();
        $price = $request->get('price');
        if($cloths){
            $i = 0;
            $catgoryData = $data['categoryId'];
            $data = $request->all();
            
            foreach($data['categoryId'] as $catgoryData){
                $cloth_prices = ClothPrice::find($catgoryData);
                $cloth_prices->price = $price[$i];
                $cloth_prices->save();
                $i++;
             }
         }
        return redirect('cloth');
    }
    public function delete_cloth($id)
    {
        $delete=Cloth::find($id);
        $delete->delete();
        return redirect('cloth');
    }
    
}
