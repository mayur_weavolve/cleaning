<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator; 
use App\User;
use App\UserDetail;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function list()
    { 
        $list=User::paginate(5);
        $data=[];
        $data['users']=$list;
        return view('users.list',$data);
    }
    public function serach(Request $request)
    {
        $order = "first_name";
        if ($request->get('order'))
        {
            $order = $request->get('order');
        }
        $serach = $request->get('search');
        $per_page = $request->get('per_page');
        $list = User::where('first_name','like',"{$serach}%")
                    ->orderBy("{$order}","asc")->paginate($per_page);
        $data=[];
        $data['users']=$list;
        return view('users.list',$data);
    }
    public function user_delete($id)
    { 
        $delete=User::find($id);
        $delete->delete();
        return redirect('user');
    }
    public function user_detail($id)
    { 
        $list=User::find($id);
        return view('users.user_detail',['users' => $list]);
    }
    public function add_user()
    { 
        return view('users.add');
    }
    public function save_user(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'first_name' => 'required', 
            'last_name' => 'required', 
            'mobileno' => 'required', 
            'email' => 'required',
            'password' => 'required', 
            'c_password' => 'required|same:password',        
        ]);
        if ($validator->fails()) {
            return redirect(route('add_user'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $user = new User();
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->mobileno = $data['mobileno'];
        $user->email = $data['email'];
        $user->password = bcrypt($data['password']);
        $user->status = "active";
        $user->role_id ="3";
        $user->save();
        
        return redirect('user');
    }
    public function user_profile()
    {
        $user = Auth::user();
        $list=UserDetail::where("user_id", $user->id)->get()->first();
        $data=[];
        $data['detail']=$list;
        return view('users.user_profile',$data);        
    }

}

