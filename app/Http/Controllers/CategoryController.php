<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Validator;

class CategoryController extends Controller
{
    
    public function category()
    {
        $list=Category::paginate(2);
        $data=[];
        $data['categories']=$list;
        return view('admin.category.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        $list=Category::where('name','like',"{$serach}%")->paginate(2);
        $data=[];
        $data['categories']=$list;
        return view('admin.category.list',$data);
    }
    public function add_category()
    {
        return view('admin.category.add');
    }
    public function save_category(Request $request)
    {
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'status' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_category'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $categorys = new Category();
        $categorys->name = $data['name'];
        $categorys->status = $data['status'];

        if ($request->file('image')==null){
            $categorys->save();
        }
        else{
            $categorys->image = $request->file('image')->hashName('image');
            $request->file('image')
            ->store('image');
            $categorys->save();
        }
        return redirect('category');
    }
    public function delete_category($id)
    {
        $delete=Category::find($id);
        $delete->delete();
        return redirect('category');
    }
    public function update_category($id)
    {
        $categories = Category::where('id',$id)->get();
        $data  = [];
        $data['categories'] = $categories;
        return view('admin.category.update',$data);
    }
    public function updatesave_category($id, Request $request){
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'status' => 'required',  
            
        ]);
        if ($validator->fails()) {
            return redirect(route('update_category'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $categorys = Category::find($id);
        $categorys->name = $data['name'];
        $categorys->status = $data['status'];
        if ($request->file('image')==null){
            $categorys->save();
        }
        else{
            $categorys->image = $request->file('image')->hashName('image');
            $request->file('image')
            ->store('image');
            $categorys->save();
        }
        return redirect('category');
    }
}
