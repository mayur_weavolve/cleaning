<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Driver;
use App\User;
use Validator;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    public function driver()
    {
        $user = Auth::user();
        $list=Driver::where("user_id", $user->id)->paginate(2);;
        $data=[];
        $data['drivers']=$list;
        return view('admin.driver.list',$data);
    }
    public function serach(Request $request)
    {
        $serach = $request->get('search');
        $user = Auth::user();
        if($user->role_id=='1')
        {
            $list=Driver::paginate(2);
            $data=[];
            $data['drivers']=$list;
            return view('admin.driver.list',$data);
        }
        else{
            $list=Driver::where("user_id", $user->id)->paginate(2);
            $data=[];
            $data['drivers']=$list;
            return view('admin.driver.list',$data);
        }
    }
    public function add_driver()
    {
        return view('admin.driver.add');
    }
    public function save_driver(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [ 
            'first_name' => 'required',
            'last_name' => 'required',
            'mobileno' => 'required',
            'email' => 'required|email|unique:users', 
            'password' => 'required',
            'c_password' => 'required|same:password',     
            
        ]);
        if ($validator->fails()) {
            return redirect(route('add_driver'))
                        ->withErrors($validator)
                        ->withInput();
        }
        $data = $request->all();
        $drivers = new User();
        $drivers->first_name = $data['first_name'];
        $drivers->last_name = $data['last_name'];
        $drivers->mobileno = $data['mobileno'];
        $drivers->status = $data['status'];
        $drivers->role_id = '4';
        $drivers->email = $data['email'];
        $drivers->password = bcrypt($data['password']);
        $drivers->save();

        $add = new Driver;
        $add->user_id = $user->id;
        $add->driver_id = $drivers->id;
        $add->save();
        return redirect('driver');
    }
    public function delete_driver($id)
    {
        $delete=Driver::find($id);
        $delete->delete();
        return redirect('driver');
    }
    public function update_driver($id)
    {
        $drivers = Driver::where('id',$id)->get();
        $data  = [];
        $data['drivers'] = $drivers;
        return view('admin.driver.update',$data);
    }
    public function updatesave_driver($id, Request $request){

        $data = $request->all();
        $drivers = Driver::find($id);
        $drivers->name = $data['name'];
        $drivers->mobile_no = $data['mobile_no'];
        $drivers->status = $data['status'];
        $drivers->save();
        return redirect('driver');
    }
}
