<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Laundry;
use Illuminate\Support\Facades\Auth;
use App\User;

class LaundryController extends Controller
{
    public $successStatus = 200;
    public function laundry_add(Request $request)
    { 
        $user = Auth::user();
        $validator = Validator::make($request->all(),[  
            'name' => 'required', 
            'mobile_no'=>'required',
            'address'=>'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'status'=>'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $data = $request->all();
        $data['user_id'] = $user->id;
        $laundry_add = Laundry::create($data);

        return response()->json(['success'=>$laundry_add], $this-> successStatus);
    }
    public function laundry_list(Request $request)
    { 
        $user = Auth::user();
        $list = Laundry::where("user_id",$user->id)->get();
        if($list){
            return response()->json(['status' => 'success', 'data' => $list, 'code' => 200]);
        }else{
            return response()->json(['status' => 'error', 'data' => 'null', 'code' => $this->successStatus]);
        }
    }
    public function user_laundry_list()
    { 
        $list = Laundry::get();
        if($list){
            return response()->json(['status' => 'success', 'data' => $list, 'code' => 200]);
        }else{
            return response()->json(['status' => 'error', 'data' => 'null', 'code' => $this->successStatus]);
        }
    }
    
}
