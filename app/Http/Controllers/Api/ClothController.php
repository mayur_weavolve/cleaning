<?php

namespace App\Http\Controllers\Api;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cloth;
use App\ClothPrice;

class ClothController extends Controller
{
    public $successStatus = 400;
    public function cloth_list(Request $request)
    { 
        $list = Cloth::with('cloth_price.category')->get();
        if(count($list) > 0){
            return response()->json(['status' => 'success', 'data' => $list, 'code' => 200]);
        }else{
            return response()->json(['status' => 'error', 'data' => 'null', 'code' => $this->successStatus]);
        }
         
    } 
    
}
