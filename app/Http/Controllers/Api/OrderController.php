<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use App\Order;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\OrderItem;

class OrderController extends Controller
{
    public $successStatus = 200;
    public function order(Request $request)
    { 
        $user = Auth::user();
        $validator = Validator::make($request->all(),[ 
            'location' => 'required', 
            'latitude'=>'required',
            'longitude'=>'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $data = $request->all();
        $data['user_id'] = $user->id;
        $data['name'] = $user->name;
        $data['status'] = 'pending';
        $order = Order::create($data);
        $total = 0;
            foreach($data['order_items'] as $orderitem){
                foreach($orderitem['prices'] as $prices){
                    if($prices){
                        $item = [];
                        $item['order_id'] = $order->id;
                        $item['category_info'] = $prices['category_info'];
                        $item['price'] = $prices['price'];
                        $item['quantity'] = $prices['quantity'];
                        $total += $prices['price'] * $prices['quantity'];
                        $orderitem = OrderItem::create($item);
                    }
                }   
            }
            $order->price = $total;
            $order->save();
            $message['message'] = "order create sucessfully";
        return response()->json(['success'=>$message], $this-> successStatus);         
    } 
}