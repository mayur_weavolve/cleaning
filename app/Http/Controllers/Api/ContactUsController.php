<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use App\ContactUs;

class ContactUsController extends Controller
{
    public $successStatus = 200;
    public $error = 401;
    public function add(Request $request)
    {
        $validator = Validator::make($request->all(),[  
            'name' => 'required', 
            'email'=>'required',
            'contact_no'=>'required',
            'subject'=>'required',
            'message'=>'required',
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        $data = $request->all();
        $contactus = ContactUs::create($data);
        $success['id'] =  $contactus->id;
        $success['message']="Contactus add successfully.";
        return response()->json(['success'=>$success], $this-> successStatus);
    }
}
