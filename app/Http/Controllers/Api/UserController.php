<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\UserDetail;

class UserController extends Controller
{
    public $successStatus = 200;
    public $error = 401;
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [  
            'email' => 'required', 
            'password' => 'required', 
            
        ]);
        if ($validator->fails()) {  
            return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);          
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user();
            $data['id'] = $user->id;
            $data['first_name'] =  $user->first_name;
            $data['last_name'] =  $user->last_name;
            $data['email'] =  $user->email;  
            return response()->json([
                    "success:"=>true,
                    "data"=>[
                            'token'=> $user->createToken('MyApp')-> accessToken,
                            "data" =>$data
                        ]
                    ], $this-> successStatus); 
        }
        
        else{
            return response()->json(['status' => 'error','error'=>'email or password does not match','code' => $this->error]);
        }
    }
/** 
* Register api 
* 
* @return \Illuminate\Http\Response 
*/ 
    public function register(Request $request) 
    { 
        
        $validator = Validator::make($request->all(), [ 
            'first_name' => 'required',
            'last_name' => 'required', 
            'mobileno' => 'required',
            'email' => 'required|email|unique:users', 
            'password' => 'required',
            'c_password' => 'required|same:password', 
            
        ]);
        if ($validator->fails()) {   
            return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);          
        }
        
        else
            $input = $request->all(); 
            $input['password'] = bcrypt($input['password']);
            $input['role_id'] = '2';
            $input['status'] = 'active';
            $user = User::create($input); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $success['id'] =  $user->id;
            $success['first_name'] =  $user->first_name;
            $success['email'] =  $user->email;
            $success['message']="User Register successfully.";
           
            return response()->json(['success'=>$success], $this-> successStatus); 
        
    }
    public function update_name(Request $request)
    {
        $validator = Validator::make($request->all(), [  
            'first_name' => 'required',
            'last_name' => 'required',  
        ]);
        if ($validator->fails()) {  
            return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);          
        }
         
            $user = Auth::user();
            $user->first_name=$request->input('first_name');
            $user->last_name=$request->input('last_name');
            $user->save();
            $success['first_name'] =  $user->first_name;
            $success['email'] =  $user->email;  
            $success['message']="Name has been Change successfully.";
            return response()->json(['success = true' => $success], $this-> successStatus);
    }
    public function update_email(Request $request)
    { 
        if(Auth::user()->email == $request->get('email'))
        {
            $success['message']="email has been Change successfully.";
            return response()->json(['success = true' => $success], $this-> successStatus);
        }
        else{
            $user = Auth::user();
            $validator = Validator::make($request->all(), [  
                    'email' => 'required|email|unique:users', 
            ]);
            
            if ($validator->fails()) {  
            return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);          
            }
            $user->email=$request->input('email');
            $user->save();
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;  
            $success['message']="email has been Change successfully.";
            return response()->json(['success = true' => $success], $this-> successStatus);
        }
       
    }
    public function update_mobileno(Request $request)
    {
        $user = Auth::user();
        $userdata = UserDetail::where("user_id", $user->id)->get()->first();
        if($userdata)
        {
            $validator = Validator::make($request->all(), [  
                'mobileno' => 'required',  
            ]);
            if ($validator->fails()) {  
                return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);            
            }
            $userdata->mobile_no=$request->input('mobile_no');
            $user->mobileno=$request->input('mobileno');
            $user->save();
            $userdata->save();
            $success['first_name'] =  $user->first_name;
            $success['email'] =  $user->email;
            $success['mobileno'] =  $userdata->mobileno;            
            $success['message']="Mobile No has been Change successfully.";
                return response()->json(['success = true' => $success], $this-> successStatus);
        }
        else{
                $validator = Validator::make($request->all(), [  
                'mobileno' => 'required', 
            ]);
            if ($validator->fails()) {  
                return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);            
            }
            $data = $request->all();
            $data['user_id'] = $user->id;
            $data['mobileno'] = $request['mobileno'];
            $userdata = UserDetail::create($data);;  
            $success['message']="Mobile No has been Add successfully.";
                return response()->json(['success = true' => $success], $this-> successStatus);
        } 
    }
    public function update_address(Request $request)
    {
        $user = Auth::user();
        $userdata = UserDetail::where("user_id", $user->id)->get()->first();
        if($userdata)
        {
            $validator = Validator::make($request->all(), [  
                'address1' => 'required', 
                'address2' => 'required', 
                'city' => 'required', 
                'state' => 'required', 
                'country' => 'required', 
            ]);
            if ($validator->fails()) {  
                return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);            
            }
            $userdata->address1=$request->input('address1');
            $userdata->address2=$request->input('address2');
            $userdata->city=$request->input('city');
            $userdata->state=$request->input('state');
            $userdata->country=$request->input('country');
            $userdata->save();
            $success['name'] =  $user->name;
            $success['email'] =  $user->email;
            $success['address1'] =  $userdata->address1;
            $success['address2'] =  $userdata->address2;
            $success['city'] =  $userdata->city;
            $success['state'] =  $userdata->state;
            $success['country'] =  $userdata->country;            
            $success['message']="Address has been Change successfully.";
                return response()->json(['success = true' => $success], $this-> successStatus);
        }
        else{
            $validator = Validator::make($request->all(), [  
                'address1' => 'required', 
                'address2' => 'required', 
                'city' => 'required', 
                'state' => 'required', 
                'country' => 'required', 
            ]);
            if ($validator->fails()) {  
                return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);            
            }
            $data = $request->all();
            $data['user_id'] = $user->id;
            $data['address1'] = $request['address1'];
            $data['address2'] = $request['address2'];
            $data['city'] = $request['city'];
            $data['state'] = $request['state'];
            $data['country'] = $request['country'];
            $userdata = UserDetail::create($data);;  
            $success['message']="Address has been Add successfully.";
                return response()->json(['success = true' => $success], $this-> successStatus);
        } 
    }
    public function change_password(Request $request)
    {
        $data = $request->all();
        $user = Auth::user();
        if (!(Hash::check($request->get('current_password'), $user->password))) {
            return response()->json(['status'=> 'error', "data"=> null,"message" =>"Your current password does not matches with the password you provided. Please try again."]);
        }
        
        if(strcmp($request->get('current_password'), $request->get('new_password')) == 0){
            return response()->json(['status'=> 'error', "data"=> null,"message" => "New Password cannot be same as your current password. Please choose a different password."]);
        }
        
        $validatedData = $request->validate([
        'current_password' => 'required',
        'new_password' => 'required',
        "c_password"=> 'required|same:new_password',
        ]);
        
        $user->password = bcrypt($request->get('new_password'));
        $user->save();
        return response()->json(['status'=> 'success','data' =>  $user,"message" =>"Password changed successfully !"]);
    }
}
