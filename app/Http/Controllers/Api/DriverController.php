<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Driver;
use App\User;
use App\Order;
use App\OrderItem;
use Illuminate\Support\Facades\Auth;


class DriverController extends Controller
{
    public $successStatus = 200;
    public $error = 401;

    public function list_request(Request $request)
    {
        $order = "created_at";
        if ($request->get('order'))
        {
            $order = $request->get('order');
        }
        $user = Auth::user();
        $list = Order::where("driver_id",$user->id)
                        ->orderBy("{$order}","asc")->get();
        $message['message'] = "order List";
        return response()->json(['status'=>'success','data'=>$list,'message'=>$message], $this-> successStatus);         
        
    }
    public function order_detail($id)
    {
        $user = Auth::user();
        $order = Order::find($id);
        $list = OrderItem::where('order_id',$order->id)->get();
        $message['message'] = "order List";
        return response()->json(['status'=>'success','data'=>$list,'message'=>$message], $this-> successStatus);         
        
    }
    public function update_mobileno(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [  
            'mobileno' => 'required',  
        ]);
        if ($validator->fails()) {  
            return response()->json(['status' => 'error', 'error'=>$validator->errors(), 'code' => $this->error,'message'=>'Unauthorised'],$this->error);            
        }
        $user->mobileno=$request->input('mobileno');
        $user->save();
        $success['first_name'] =  $user->first_name;
        $success['email'] =  $user->email;
        $success['mobile_no'] =  $user->mobile_no;            
        $success['message']="Mobile No has been Change successfully.";
            return response()->json(['success = true' => $success], $this-> successStatus);
    }
    public function update_order_status(Request $request,$id)
    {
        $user = Auth::user();
        $order = Order::find($id);
        if($user)
        {
            $order->pickup_status = 'pickup';
            $order->save();
            $message['message'] = "order Status Update Successfully";
        }
        return response()->json(['status'=>'success','message'=>$message], $this-> successStatus);         
    }
}
