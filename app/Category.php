<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
       'id','name', 'status','image', 
    ];
    public function cloth_price()
    {
        return $this->hasMany('App\ClothPrice');
    }
    public function cloth()
    {
        return $this->hasMany('App\Cloth');
    }
}
