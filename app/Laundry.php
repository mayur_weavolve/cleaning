<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laundry extends Model
{
    protected $fillable = [
        'id','user_id','name','mobile_no','address','latitude','longitude','status'

    ];
}
