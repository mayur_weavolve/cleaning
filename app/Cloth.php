<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cloth extends Model
{
    protected $fillable = [
        'id','name', 'image', 
    ];
    
    public function cloth_price()
    {
        return $this->hasMany('App\ClothPrice');
    }

}
