<!DOCTYPE html>

<html lang="en">

	<!-- begin::Head -->
	<head>
		<meta charset="utf-8" />
		<title>Cleaning | Login Page</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

		<!--begin::Web font -->
		<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
		<script>
			WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

		<!--end::Web font -->

		<!--begin:: Global Mandatory Vendors -->
		<link href="<?php echo URL('/'); ?>/assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Mandatory Vendors -->

		<!--begin:: Global Optional Vendors -->
		<link href="<?php echo URL('/'); ?>/assets/vendors/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/ion-rangeslider/css/ion.rangeSlider.skinFlat.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/chartist/dist/chartist.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/sweetalert2/dist/sweetalert2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/flaticon/css/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/metronic/css/styles.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo URL('/'); ?>/assets/vendors/vendors/fontawesome5/css/all.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Styles -->
		<link href="<?php echo URL('/'); ?>/assets/demo/base/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--RTL version:<link href="../../../assets/demo/base/style.bundle.rtl.css" rel="stylesheet" type="text/css" />-->

		<!--end::Global Theme Styles -->
		<link rel="shortcut icon" href="<?php echo URL('/'); ?>/assets/demo/media/img/logo/favicon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

		<!-- begin:: Page -->
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
				<div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
					<div class="m-stack m-stack--hor m-stack--desktop">
						<div class="m-stack__item m-stack__item--fluid">
							<div class="m-login__wrapper">
								<div class="m-login__logo">
									<a href="#">
										<img src="<?php echo URL('/'); ?>/assets/app/media/img/logos/logo-2.png">
									</a>
								</div>
								<div class="m-login__signin">
									<div class="m-login__head">
										<h3 class="m-login__title">Sign In To Admin</h3>
									</div>
                                    <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="form-group m-form__group">
											<input class="form-control m-input"  id="email" type="text" placeholder="Id" name="email" value="{{ old('email') }}" autocomplete="email" autofocus required>
											@error('email')
                                    			<span class="help-block" role="alert">
                                        			<strong>{{ $errors->first("email") }}</strong>
                                    			</span>
                                            @enderror
										</div>
                                        <div class="form-group m-form__group">
											<input class="form-control m-input m-login__form-input--last" id="password" type="password" placeholder="Password" name="password" required autocomplete="current-password">
											@error('password')
                                    		<span class="help-block" role="alert">
                                        		<strong>{{ $errors->first('password') }}</strong>
                                    		</span>
                                			@enderror
										</div>

                                        <!-- <div class="form-group row">
                                            <div class="col-md-6 offset-md-4">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                    <label class="form-check-label" for="remember">
                                                        {{ __('Remember Me') }}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Login') }}
                                                </button>

                                                @if (Route::has('password.request'))
                                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                                        {{ __('Forgot Your Password?') }}
                                                    </a>
                                                @endif
                                            </div>
                                        </div> -->
                                        <div class="m-login__form-action">
											<button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Sign In</button>
										</div>

                                    </form>
                                </div>
							</div>
						</div>
					</div>
				</div>
                <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center" style="background-image: url(<?php echo URL('/'); ?>/assets/app/media/img//bg/bg-4.jpg)">
					<div class="m-grid__item">
						<h3 class="m-login__welcome">Join Our Community</h3>
						<p class="m-login__msg">
							Lorem ipsum dolor sit amet, coectetuer adipiscing<br>elit sed diam nonummy et nibh euismod
						</p>
					</div>
				</div>
			</div>
		</div>
        
        <!-- end:: Page -->

		<!--begin:: Global Mandatory Vendors -->
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/wnumb/wNumb.js" type="text/javascript"></script>

		<!--end:: Global Mandatory Vendors -->

        <!--begin:: Global Optional Vendors -->
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js" type="text/javascript"></script>
        <script src="<?php echo URL('/'); ?>/assets/vendors/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/handlebars/dist/handlebars.js" type="text/javascript"></script>
        <script src="<?php echo URL('/'); ?>/assets/vendors/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/inputmask/dist/inputmask/inputmask.phone.extensions.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/nouislider/distribute/nouislider.js" type="text/javascript"></script>
        <script src="<?php echo URL('/'); ?>/assets/vendors/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/dropzone/dist/dropzone.js" type="text/javascript"></script>
        <script src="<?php echo URL('/'); ?>/assets/vendors/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>
        <script src="<?php echo URL('/'); ?>/assets/vendors/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js/framework/components/plugins/forms/jquery-validation.init.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/jstree/dist/jstree.js" type="text/javascript"></script>
        <script src="<?php echo URL('/'); ?>/assets/vendors/raphael/raphael.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/morris.js/morris.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/chartist/dist/chartist.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js/framework/components/plugins/charts/chart.init.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/vendors/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="<?php echo URL('/'); ?>/assets/vendors/js/framework/components/plugins/base/sweetalert2.init.js" type="text/javascript"></script>

        <!--end:: Global Optional Vendors -->

		<!--begin::Global Theme Bundle -->
		<script src="<?php echo URL('/'); ?>/assets/demo/base/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Global Theme Bundle -->

        <!--begin::Page Scripts -->
		<!-- <script src="<?php echo URL('/'); ?>/assets/snippets/custom/pages/user/login.js" type="text/javascript"></script> -->

        <!--end::Page Scripts -->

        </body>

	<!-- end::Body -->
</html>

