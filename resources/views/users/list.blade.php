@extends('layouts.admin')

@section('content')
<div class="m-content">
	<!-- <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div> -->
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						List All Order
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('add_user') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>New User Add</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			</ul>
	    </div>	
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('user')}}" method = "get" autocomplete="off">
					<div class="row">
						<div class = "col-md-2" >
							<select style = "padding: 9px" class="form-control" name="order">
							<option value = "">Order By</option>
							<option value = "id">Id</option>
							<option value = "first_name">First Name</option>
							<option value = "last_name">Last Name</option>
							</select>
						</div>
						<div class = "col-md-6">
							<input type="search" name="search"class="form-control" placeholder="Search"><br>
							<span class="glyphicon glyphicon-search form-control-feedback"></span>
						</div>
						<div class="col-md-2">			
								<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
						<div class="col-md-2">			
						<select style = "padding: 9px" class="form-control" name="per_page">
							<option value = "">Per Page</option>
							<option value = "2">2</option>
							<option value = "3">3</option>
							<option value = "5">5</option>
							<option value = "10">10</option>
							</select>
						</div>
					</div>
				</form>
			</div>
		</div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
                    <th >User Id</th>
					<th>first Name</th>
					<th>Last Name</th>
					<th>Email_Id</th>
					<th>Stauts</th>
					<th>Role_Id</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>
                <?php foreach ($users as $user) { ?>
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
                        <td>{{ $user->email}}</td>
                        <td>{{ $user->status}}</td> 
						<td>{{ $user->role_id}}</td>
                        <td>
                            <i class="m-menu__link-icon fa fa-edit">
                            <a href="{{ route('user_detail',[$user->id])}}">Detail 
                            </i></a>&nbsp;
                            <i class="m-menu__link-icon fa fa-trash-alt">
                                <a href="{{route('user_delete',[$user->id])}}">Delete
                            </i></a>
                        </td>
                    </tr>
                <?php } ?>							
			</tbody>
		</table>
		{{$users->links()}}
	</div>
</div>
</div>
</div>
</div>
        
@endsection