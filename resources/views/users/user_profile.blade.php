@extends('layouts.admin')

@section('content')
<div class="m-content">
    <div class="row">
		<div class="col-xl-3 col-lg-4">
			<div class="m-portlet m-portlet--full-height  ">
				<div class="m-portlet__body">
					<div class="m-card-profile">
						<div class="m-card-profile__title m--hide">
							Your Profile
						</div>
						<div class="m-card-profile__pic">
							<div class="m-card-profile__pic-wrapper">
								<img src="../assets/app/media/img/users/user4.jpg" alt="" />
							</div>
						</div>
                        <div class="m-card-profile__details">
                            <span class="m-card-profile__name">{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</span>
                            <a href="" class="m-card-profile__email m-link">{{ Auth::user()->email }}</a>
                        </div>
					</div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                        <li class="m-nav__separator m-nav__separator--fit"></li>
                        <li class="m-nav__section m--hide">
                            <span class="m-nav__section-text">Section</span>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-profile-1"></i>
                                <span class="m-nav__link-title">
                                    <span class="m-nav__link-wrap">
                                        <span class="m-nav__link-text">My Profile</span>
                                    </span>
                                </span>
                            </a>
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                <span class="m-nav__link-text">Messages</span>
                            </a>
                        </li>
                        
                    </ul>				
				</div>
			</div>
		</div>
		<div class="col-xl-9 col-lg-8">
			<div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
				<div class="m-portlet__head">
					<div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    Update Profile
                                </a>
                            </li>
                        </ul>
					</div>				
				</div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                    
                        <form class="m-form m-form--fit m-form--label-align-right" action="">
                        
                            <div class="m-portlet__body">
                            
                                <div class="form-group m-form__group m--margin-top-10 m--hide">
                                    <div class="alert m-alert m-alert--default" role="alert">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">1. Personal Details</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">FIrst Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ Auth::user()->first_name }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Last Name</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ Auth::user()->last_name }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Email</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ Auth::user()->email }}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Phone No.</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ $detail->mobile_no}}">
                                    </div>
                                </div>
                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                <div class="form-group m-form__group row">
                                    <div class="col-10 ml-auto">
                                        <h3 class="m-form__section">2. Address</h3>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Address 1</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ $detail->address1}}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Address 2</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ $detail->address2}}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">City</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ $detail->city}}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">State</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ $detail->state}}">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Country</label>
                                    <div class="col-7">
                                        <input class="form-control m-input" type="text" value="{{ $detail->country}}">
                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2">
                                        </div>
                                        <div class="col-7">
                                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Save changes</button>&nbsp;&nbsp;
                                            <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                            
                        </form>
                        	
                    </div>
                    
                </div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
@endsection