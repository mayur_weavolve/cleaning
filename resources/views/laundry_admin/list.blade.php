@extends('layouts.admin')
@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Laundry
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('add_laundry')}}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Add New Laundry</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<div class = "col-md-12">
				<form action = "{{route('laundry')}}" method = "get">
					<div class="row">
						<div class = "col-md-10 text-right">
							<input type="search" name="search"class="form-control" placeholder="Search"><br>
						</div>
						<div class="col-md-2 text-right">			
								<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
					</div>
				</form>
			</div>
		</div>
	<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<!-- <th>Cloth ID</th> -->
					<th>Id</th>
					<th>Laundry_id</th>
					<th>name</th>
                    <th>Mobile_No</th>
                    <th>Address</th>
					<th>Latitude</th>
					<th>Longitude</th>
                    <th>Status</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
            <?php foreach ($laundries as $laundry) { ?>
              <tr>
                  <td> <?php echo $laundry->id ;?> </td>
				  <td>{{$laundry->user_id}}</td>
                  <td>{{$laundry->name}}</td>
                  <td>{{$laundry->mobile_no}}</td>
				  <td>{{$laundry->address}}</td>
                  <td>{{$laundry->latitude}}</td>
                  <td>{{$laundry->longitude}}</td>
				  <td>{{$laundry->status}}</td>
					<td>
						<i class="m-menu__link-icon fa fa-edit">
							<a href="{{ route('edit_laundry',[$laundry->id])}}">Edit</a>
						</i>&nbsp;&nbsp;
						<i class="m-menu__link-icon fa fa-trash-alt">
							<a href="{{route('delete_laundry',[$laundry->id])}}">Delete</a>
						</i>
					</td>
              	</tr>
                  <?php } ?>				
			</tbody>
		</table>
		{{$laundries->links()}}
	</div>
</div>
</div>
</div>
</div>

           
@endsection
