@extends('layouts.admin')
@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						List All Order
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('order') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
			
	</div>
	<div class="m-portlet__body">

	<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<th>order_id</th>
					<th>price</th>
                    <th>quantity</th>
                    <th>category_info</th>
					<th>cloth_info</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($order->orderitem as $orderitem){ ?>
				<td>{{$orderitem->order_id}}</td>
				<td>{{$orderitem->price}}</td>
				<td>{{$orderitem->quantity}}</td>
				<td>{{$orderitem->category_info}}</td>
				<td></td>

				<tr>
              	 </tr> 
			<?php } ?>		
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>

           
@endsection
