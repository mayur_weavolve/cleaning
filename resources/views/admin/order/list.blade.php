@extends('layouts.admin')
@section('content')
<div class="m-content">
	<!-- <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div> -->
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						List All Order
					</h3>
				</div>
			</div>
			
	</div>
	<div class="m-portlet__body">
        <div class = "row">
            <div class = "col-md-9 text-right">
            <input type="text" name="search" class="form-control" placeholder="Search" style = "width:135%"><br>
            </div>
        </div>
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
                    <th >Order Id</th>
					<th>User Name</th>
					<th>User_id</th>
					<th>Location</th>
                    <th>Status</th>
                    <th>Driver Id</th>
                    <th>Price</th>
                    <th>Relese Date</th>
                    <th>Relese Status</th>
					<th>Latitude</th>
					<th>Longitude</th>
                    <th>PickUp Status</th>
                    <th>PickUp Date</th>
                    <th>Action</th>
				</tr>
			</thead>
			<tbody>
                <?php foreach ($orders as $order) { ?>
                    <tr>
                        <td>{{ $order->id }}</td>
                        <td>{{ $order->name }}</td>
                        <td>{{ $order->user_id}}</td>
                        <td>{{ $order->location}}</td> 
                        <td>{{ $order->status}}</td>
                        <td></td>
                        <td>{{ $order->price}}</td>
                        <td></td>
                        <td></td>
                        <td>{{ $order->latitude}}</td>
                        <td>{{ $order->longitude}}</td>
                        <td>{{ $order->pickup_status}}</td>
                        <td></td>
                        <td>
                            <i class="m-menu__link-icon fa fa-edit">
                            <a href="{{ route('order_item',[$order->id])}}">Detail 
                            </i></a>&nbsp;
                            <i class="m-menu__link-icon fa fa-trash-alt">
                                <a href="">Delete
                            </i></a>
                        </td>
                    </tr>
                <?php } ?>							
			</tbody>
		</table>
	</div>
</div>
</div>
</div>
</div>
        
@endsection