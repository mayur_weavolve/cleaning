@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Update Cloth
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('cloth') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>
					


	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form"  method="post"  enctype="multipart/form-data" action="">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
					<?php foreach ($cloths as $cloth) { ?>
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Cloth Name:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="name" class="form-control m-input" placeholder="{{ $cloth->name}}" value="{{ $cloth->name}}" required>
										@error('name')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('name') }}</strong>
                        				</span>
                    					@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Description:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="description" class="form-control m-input" placeholder="{{ $cloth->description}}" value="{{ $cloth->description}}" required>
										@error('description')
                       					<span class="help-block" role="alert">
                           					<strong>{{ $errors->first('description') }}</strong>
                        				</span>
                    					@enderror
									</div>
							</div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Image:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="file" name="image" class="form-control m-input" placeholder="" value="">
									</div>
							</div>
							<div class="m-separator m-separator--dashed m-separator--lg"></div>
								<div class="m-form__section">
									<div class="m-form__heading">
										<h3 class="m-form__heading-title">
											<b><u>Price</u></b>
										</h3>
									</div>
						
								<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
									<thead>
										<tr>
											<th>Category Name</th>
											<th>Price</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ($cloth->cloth_price as $category) { ?>
											
										<tr>
											<td>{{$category->category->name}} <input type="hidden" name="categoryId[]" value="{{ $category->id}}" ></td>
											<td><input type="text" name="price[]" class="form-control m-input" placeholder=""  value="{{ $category->price}}"  ></td> 
										</tr>
											<?php } ?>
											
									</tbody>
								</table>
							</div>	
							<?php } ?>							
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
										<span>Submit</span>
									</span>
								</button>							
							</div>
						</div>
						 
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
           
@endsection
