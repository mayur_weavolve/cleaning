@extends('layouts.admin')

@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Driver
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			<ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('add_driver') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-plus"></i>
						    <span>Add New Driver</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item">
				
			</ul>
	    </div>
	</div>
	<div class="m-portlet__body">
		<div class = "row">
			<!-- <div class = "col-md-12">
				<form action = "{{route('driver')}}" method = "get">
					<div class="row">
						<div class = "col-md-10 text-right">
							<input type="search" name="search"class="form-control" placeholder="Search"><br>
						</div>
						<div class="col-md-2 text-right">			
								<button type = "submit" class ="btn btn-primary">Search</button><br>
						</div>
					</div>
				</form>
			</div> -->
		</div>
	<!--begin: Datatable -->
		<table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
			<thead>
				<tr>
					<th>ID</th>
					<th>Driver ID</th>
					<th>Laundry Id</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach ($drivers as $driver) { ?>
              <tr>
                   <td> <?php echo $driver->id ;?> </td>
                  <td>{{ $driver->driver_id}}</td>
                  <td>{{ $driver->user_id}}</td> 
                  <td>
					<i class="m-menu__link-icon fa fa-edit">
					  <a href="{{route('update_driver',[$driver->id])}}">Edit
					</i></a>&nbsp;&nbsp;
					<i class="m-menu__link-icon fa fa-trash-alt">
						  <a href="{{route('delete_driver',[$driver->id])}}">Delete
					</i></a>
				  </td>
              	</tr>
        	<?php } ?>							
			</tbody>
		</table>
		{{$drivers->links()}}
	</div>
</div>
</div>
</div>
</div>

           
@endsection
