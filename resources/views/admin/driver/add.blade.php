@extends('layouts.admin')
@section('content')
<div class="m-content">
	<div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30" role="alert">	
	</div>
	<div class="m-portlet m-portlet--mobile">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<h3 class="m-portlet__head-text">
						Add New Driver
					</h3>
				</div>
			</div>
			<div class="m-portlet__head-tools">
			    <ul class="m-portlet__nav">
				<li class="m-portlet__nav-item">
					<a href="{{ route('driver') }}" class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
						<span>
							<i class="la la-reply"></i>
						    <span>Back</span>
						</span>
					</a>
				</li>
				<li class="m-portlet__nav-item"></li>
				<li class="m-portlet__nav-item"></li>
			    </ul>
	        </div>
	    </div>
					


	<form class="m-form m-form--label-align-left- m-form--state-" id="m_form"  method="post" action="{{route('add_driver')}}">
	{{csrf_field() }}
		<div class="m-portlet__body">
			<div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
				<div class="row">
					<div class="col-xl-8 offset-xl-2">
						<div class="m-form__section">
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Driver first Name:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="first_name" class="form-control m-input" placeholder="" value="" required>
										@error('name')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('first_name') }}</strong>
                        					</span>
                    					@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Driver Last Name:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="last_name" class="form-control m-input" placeholder="" value="" required>
										@error('name')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('last_name') }}</strong>
                        					</span>
                    					@enderror
									</div>
							</div>
                            <div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Mobile No:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="mobileno" class="form-control m-input" placeholder="" value="" required>
										@error('mobile_no')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('mobile_no') }}</strong>
                        					</span>
                    					@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Email Id:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="text" name="email" class="form-control m-input" placeholder="" value="" required>
										@error('mobile_no')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('email') }}</strong>
                        					</span>
                    					@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Password:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="password" name="password" class="form-control m-input" placeholder="" value="" required>
										@error('mobile_no')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('password') }}</strong>
                        					</span>
                    					@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Confirm Password:</label>
									<div class="col-xl-8 col-lg-8">
										<input type="password" name="c_password" class="form-control m-input" placeholder="" value="" required>
										@error('mobile_no')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('c_password') }}</strong>
                        					</span>
                    					@enderror
									</div>
							</div>
							<div class="form-group m-form__group row">
								<label class="col-xl-3 col-lg-3 col-form-label">* Status:</label>
								<div class="col-xl-8 col-lg-8">
									<select name="status" class="form-control m-input" required>
										<option value="">Select</option>
										<option value="active">Active</option>
										<option value="deactive">De-active</option>											
									</select>
									@error('status')
                       					 	<span class="help-block" role="alert">
                           					 <strong>{{ $errors->first('status') }}</strong>
                        					</span>
                    				@enderror
								</div>
							</div>
							
							<div class="col-lg-8 m--align-right">
								<button class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit" >
									<span>
										<i class="la la-check"></i>&nbsp;&nbsp;
											<span>Submit</span>
									</span>
								</button>
																
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</div>
</div>
</div>
</div>
           
@endsection
