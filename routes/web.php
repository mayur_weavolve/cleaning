<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/', function () {
    return view('auth.login');
});
Route::get('home', 'HomeController@adminHome')->name('home');
Route::group(['middleware' => 'auth'], function(){
    Route::get('welcome','HomeController@index')->name('welcome');
    //---------------------------category opreation --------------------------------------

    Route::get('category','CategoryController@category')->name('category');
    Route::get('category','CategoryController@serach')->name('category');
    Route::get('admin/category/add','CategoryController@add_category')->name('add_category');
    Route::post('admin/category/add','CategoryController@save_category');
    Route::get('category/delete/{id}', 'CategoryController@delete_category')->name('delete_category');
    Route::get('admin/category/update/{id}','CategoryController@update_category')->name('update_category');
    Route::post('admin/category/update/{id}','CategoryController@updatesave_category');
    //------------------------------------------------------------------------------------
    //---------------------------driver opreation-----------------------------------------
    Route::get('admin/driver/add','DriverController@add_driver')->name('add_driver');
    Route::post('admin/driver/add','DriverController@save_driver');
    Route::get('driver','DriverController@driver')->name('driver');
    Route::get('driver','DriverController@serach')->name('driver');
    Route::get('driver/delete/{id}', 'DriverController@delete_driver')->name('delete_driver');
    Route::get('admin/driver/update/{id}','DriverController@update_driver')->name('update_driver');
    Route::post('admin/driver/update/{id}','DriverController@updatesave_driver');
    //-------------------------------------------------------------------------------------
    //---------------------------cloth opreation-------------------------------------------
    Route::get('admin/cloth/add','ClothController@add_cloth')->name('add_cloth');
    Route::post('admin/cloth/add','ClothController@save_cloth');
    Route::get('cloth/delete/{id}', 'ClothController@delete_cloth')->name('delete_cloth');
    Route::get('cloth','ClothController@cloth')->name('cloth');
    Route::get('cloth','ClothController@serach')->name('cloth');
    Route::get('admin/cloth/update/{id}','ClothController@update_cloth')->name('update_cloth');
    Route::post('admin/cloth/update/{id}','ClothController@updatesave_cloth');
    //-------------------------------------------------------------------------------------
    //---------------------------order opreation-------------------------------------------
    Route::get('order','OrderController@order_list')->name('order');
    Route::get('order_item/{id}','OrderController@order_item')->name('order_item');
    //-------------------------------------------------------------------------------------
    //---------------------------laundry opreation-------------------------------------------
    Route::get('laundry','LaundryController@list')->name('laundry');
    Route::get('laundry','LaundryController@serach')->name('laundry');
    Route::get('laundry_admin/add','LaundryController@add_laundry')->name('add_laundry');
    Route::post('laundry_admin/add','LaundryController@save_laundry');
    Route::get('laundry_admin/update/{id}','LaundryController@edit_laundry')->name('edit_laundry');
    Route::post('laundry_admin/update/{id}','LaundryController@edit_save');
    Route::get('laundry/delete/{id}', 'LaundryController@delete_laundry')->name('delete_laundry');

    Route::get('user','AdminController@list')->name('user');
    Route::get('user','AdminController@serach')->name('user');
    Route::get('user/add','AdminController@add_user')->name('add_user');
    Route::post('user/add','AdminController@save_user');
    Route::get('user/delete/{id}', 'AdminController@user_delete')->name('user_delete');
    Route::get('user_detail/{id}','AdminController@user_detail')->name('user_detail');
    Route::get('user/user_profile','AdminController@user_profile')->name('user_profile');

    
});