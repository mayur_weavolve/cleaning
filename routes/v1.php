<?php

Route::post('login', 'Api\UserController@login');
Route::post('register', 'Api\UserController@register');

Route::group(['middleware' => 'auth:api'], function(){
//-------------------profile opretion ------------------------------------
    Route::post('update_name', 'Api\UserController@update_name');
    Route::post('update_email', 'Api\UserController@update_email');
    Route::post('update_mobileno', 'Api\UserController@update_mobileno');
    Route::post('update_address', 'Api\UserController@update_address');
    Route::post('change_password', 'Api\UserController@change_password');
//------------------------------------------------------------------------
    Route::post('cloth_list', 'Api\ClothController@cloth_list');
    Route::post('order', 'Api\OrderController@order');
    Route::post('order_item', 'Api\OrderItemController@order_item');
//-------------------Laundry opretion ------------------------------------
    Route::post('laundry_add', 'Api\LaundryController@laundry_add');
    Route::post('laundry_list', 'Api\LaundryController@laundry_list');
    Route::post('user/laundry_list', 'Api\LaundryController@user_laundry_list');
//------------------------------------------------------------------------
    Route::post('contactus', 'Api\ContactUsController@add');
//-------------------Driver opretion ------------------------------------
    Route::post('driver/get_request', 'Api\DriverController@list_request');
    Route::post('driver/order_detail/{id}', 'Api\DriverController@order_detail');
    Route::post('driver/update/mobileno', 'Api\DriverController@update_mobileno');
    Route::post('driver/update/orderstatus/{id}', 'Api\DriverController@update_order_status');
//------------------------------------------------------------------------

    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');

});
