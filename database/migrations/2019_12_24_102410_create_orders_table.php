<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->Increments('id');
            $table->string("name", 255);
            $table->integer("user_id" );
            $table->string("location", 255);
            $table->string("status" );
            $table->integer("driver_id" );
            $table->integer("price");
            $table->dateTime("release_date" );
            $table->dateTime("release_status" );
            $table->double("latitude" );
            $table->double("longitude" );
            $table->dateTime("pickup_date" );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
